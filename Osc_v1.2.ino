/************************************
Oscilloscope Hat for Arduino Uno
************************************/

#include<SPI.h>
#include<Adafruit_GFX.h>
#include<Adafruit_PCD8544.h>

int dial; 
long values[84];
Adafruit_PCD8544 display = Adafruit_PCD8544(13,11,9,10,8);

void setup() {
  Serial.begin(9600);
  display.begin();
  display.setContrast(50);
}

void loop() {

  dial = analogRead(A1); //range selector for time per division
  int n=map(dial,0,1023,1,15); //map range to divisions 
  int tim=map(568,0,1023,(n-1)*1000,n*1000)-555; //range from 0-1000,1000-2000 etc
  int td = map(tim,0,14000,5,100); //range to time divisions
  
for(int i=0;i<84;i++) {
  values[i]=map(analogRead(A0),0,1023,38,1); //getting values
  delayMicroseconds(tim);
}

display.clearDisplay();
display.drawLine(0,20,84,20,BLACK); //x-axis
display.drawLine(42,0,42,39,BLACK); //y-axis
display.drawRect(0,0,84,40, BLACK);
display.setTextSize(0);
display.setTextColor(BLACK);
display.setCursor(0,40);
display.print("5Vp-p");
display.setCursor(54,40);
display.print(td);
display.setCursor(72,40);
display.print("ms");

for(int i=4;i<=38;i+=4) {
  display.drawLine(41,i,43,i,BLACK);//y-axis scale
}

for(int i=0;i<=82;i+=7) {
  display.drawLine(i,19,i,21,BLACK);//x-axis scale
}

for(int i=0;i<84;i++) {
  display.drawLine(i,values[i],i,values[i+1], BLACK); //drawing graph
  while(digitalRead(12)==HIGH) { //hold switch at GPIO12
      delay(1);
  }
}
display.display();
}
