Arduino Oscilloscope Hat

A rudimentary oscilloscope sheild like DIY circuit currently limited to one 5v 200Hz signal(max).

Components Used:

1. Arduino Uno
2. PerfBoard(20x30)
3. Nokia 5110 LCD display
4. 10k potentiometer
5. Push button
6. Header Pins

Dependencies:

1. SPI library
2. Adafruit_GFX library
3. Adafruit_PCD8544 library
